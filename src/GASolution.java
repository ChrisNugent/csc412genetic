/*
 * Written by Chris Nugent for CSC412
 *
 * GASolution
 * Represents a single member of a population for Sudoku solutions
 *
 * Offers methods for breeding, mutation, and fitness checking.
 */

import java.util.Arrays;
import java.util.Random;

public class GASolution implements Comparable<GASolution> {
    private static final int GENOME_SIZE = 81;
    private static final int MUTATION_AGGRESSION = 1;

    private Integer[] genome;
    private int cachedFitness = -1;
    private boolean hasChanged;

    public GASolution() {
        genome = new Integer[GENOME_SIZE];
        hasChanged = true;
    }

    public void randomize() {
        Random r = new Random();
        for (int i = 0; i < genome.length; i++) {
            genome[i] = r.nextInt(9) + 1;
        }
    }

    public Integer[] getGenome() {
        return genome;
    }

    public int getFitness() {
        if (hasChanged) {
            SudokuBoard sb = new SudokuBoard();
            sb.fill(genome);
            cachedFitness = sb.getFitness();
            hasChanged = false;
        }
        return cachedFitness;
    }

    public void mutate() {
        Random r = new Random();

        for (int i = 0; i < MUTATION_AGGRESSION; i++) {
            int mutatePos = r.nextInt(GENOME_SIZE);
            int mutateVal = r.nextInt(9) + 1;
            genome[mutatePos] = mutateVal;
        }
        hasChanged = true;
    }

    public GASolution crossover(GASolution mate) {
//        System.out.println("Crossover...");
        Random r = new Random();
        GASolution child = new GASolution();
        int start = 0;
        while (start < GENOME_SIZE) {
            int length = r.nextInt(GENOME_SIZE - start  + 1);
            System.arraycopy(this.genome, start, child.genome, start, length);
            start += length;

            length = r.nextInt(GENOME_SIZE - start + 1);
            System.arraycopy(mate.genome, start, child.genome, start, length);
            start += length;
        }
        return child;
    }

    public GASolution breed(GASolution mate) {
//        System.out.println("Breeding...");
        Random r = new Random();
        GASolution child = new GASolution();
        for (int i = 0; i < GENOME_SIZE; i++) {
            if (r.nextInt() % 2 == 0) {
                child.genome[i] = this.genome[i];
            } else {
                child.genome[i] = mate.genome[i];
            }
        }
        return child;
    }

    public String toString() {
        return Arrays.toString(genome);
    }

    @Override
    public int compareTo(GASolution o) {
        return getFitness() - o.getFitness();
    }

}
