/*
 * Written by Chris Nugent for CSC412
 *
 * GAPopulation
 * Represents a population of GASolutions.
 *
 * Offers methods for producing and testing new generations.
 */

import java.util.Arrays;
import java.util.Random;

public class GAPopulation {
    public static final int TOURNAMENT_SIZE = 5;
    public static final int DEFAULT_SIZE = 1000;
    public static final double IMMORTALITY_RATIO = 0;
    public static final double ANTIELITISM_RATIO = 0.05;

    private final int size;
    private GASolution[] population;


    public GAPopulation() {
        this(DEFAULT_SIZE);
    }

    public GAPopulation(int size) {
        this.size = size;
        population = new GASolution[size];
    }

    public void initialize() {
        for (int i = 0; i < population.length; i++) {
            population[i] = new GASolution();
            population[i].randomize();
        }
    }

    public int sort() {
        Arrays.sort(population);
        return population[size - 1].getFitness();
    }

    /* Choose a parents based on tournament selection */
    private GASolution selectParent() {
        Random r = new Random();
        GASolution bestParent = population[0];
        int bestFitness = 0;
        for (int i = 0; i < TOURNAMENT_SIZE; i++) {
            GASolution candidate = population[r.nextInt(size)];
            int fitness = candidate.getFitness();
            if (fitness > bestFitness) {
                bestParent = candidate;
                bestFitness = fitness;
            }
        }
        return bestParent;
    }

    public GAPopulation breed() {
        GASolution[] children = new GASolution[size];
        int startPos, finishPos, length;

        /* Preserve percentage of strongest members */
        length = (int) (size * IMMORTALITY_RATIO);
        startPos = length;
        System.arraycopy(population, 0, children, 0, length);


        /* Preserve precentage of weakest members */
        length = (int) (size * ANTIELITISM_RATIO);
        finishPos = size - length;
        System.arraycopy(population, finishPos, children, finishPos, length);

        /* Breed remaining population slots */
        for (int i = startPos; i < finishPos; i++) {
            GASolution p1 = selectParent();
            GASolution p2 = selectParent();
            children[i] = p1.breed(p2);
        }
        GAPopulation result = new GAPopulation(size);
        result.population = children;
        return result;
    }

    public GAPopulation crossover() {
        GASolution[] children = new GASolution[size];
        for (int i = 0; i < size; i++) {
            GASolution p1 = selectParent();
            GASolution p2 = selectParent();
            children[i] = p1.crossover(p2);
        }
        GAPopulation result = new GAPopulation(size);
        result.population = children;
        return result;
    }


    public void mutate(double mutateProb) {
        Random r = new Random();
        for (int i = 0; i < size; i++) {
            if (r.nextDouble() < mutateProb) {
                population[i].mutate();
            }
        }

    }

    public String getFitnessString() {
        return "This seems to be unused";
    }
}
